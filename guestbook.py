from turtle import *

names = []

for i in range(0, 4):
    names.append(textinput("Name", "Please enter your name"))

for name in names:
    write(name)
    penup()
    right(90)
    forward(50)
    left(90)
    pendown()


done()