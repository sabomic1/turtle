from turtle import *

penup()
right(180)
forward(250)
right(90)
forward(250)
right(90)
pendown()
fillcolor("red")
begin_fill()
for i in range(0,5):
    for i in range(0, 5):
        forward(100)
        right(144)
    penup()
    forward(130)
    pendown()
end_fill()
done()